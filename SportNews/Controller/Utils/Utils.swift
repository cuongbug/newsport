//
//  Utils.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import CoreData

class Utils {
    // set LanguageApp
    static func setLanguage(status: Bool) {
        UserDefaults.standard.set(status, forKey: "setLanguage")
    }
    static func getLanguage() -> Bool {
        return UserDefaults.standard.bool(forKey: "setLanguage")
    }
    // set color app
    static func setHexColor(hexString: String) {
        UserDefaults.standard.set(hexString, forKey: "hexColor")
    }
    static func getColorApp() -> String {
        return UserDefaults.standard.string(forKey: "hexColor") ?? "4693B8"
    }
    // set baseColor app
    static func setBaseColor(hexColor: String) {
        UserDefaults.standard.set(hexColor, forKey: "baseColor")
    }
    static func checkDarkMode(isOn: Bool) {
        UserDefaults.standard.set(isOn, forKey: "onDarkMode")
    }
    static func getCheckDarkMode() -> Bool {
        return UserDefaults.standard.bool(forKey: "onDarkMode") 
    }
    
    static func getBaseColor() -> String {
        return UserDefaults.standard.string(forKey: "baseColor") ?? "#202020"
    }
    // set darkmode app
    static func setDarkColor(hexString: String) {
        UserDefaults.standard.set(hexString, forKey: "darkColor")
    }
    static func setWhiteColor(hexString: String) {
        UserDefaults.standard.set(hexString, forKey: "whiteColor")
    }
    static func getWhiteColor() -> String {
        return UserDefaults.standard.string(forKey: "whiteColor") ?? "#FFFFFF"
    }
    static func getDarkModeColor() -> String {
        return UserDefaults.standard.string(forKey: "darkColor") ?? "#202020"
    }
    static func getDarkColor() -> String {
        return "#202020"
    }
    // check internet
    static func setCheckNetWork(isConected: Bool) {
        UserDefaults.standard.set(isConected, forKey: "checkNetWork")
    }
    static func getCheckNetWork() -> Bool {
        let isConected: Bool = UserDefaults.standard.bool(forKey: "checkNetWork")
        return isConected
    }
    // Toát message
    static func showMessage(message: String, font: UIFont) {
        
        let width: CGFloat = UIScreen.main.bounds.size.width - 32
        let widthScreen: CGFloat = UIScreen.main.bounds.size.width
        let toastLabel = UILabel(frame: CGRect(x: widthScreen/2 - width/2, y: UIScreen.main.bounds.size.height / 2, width: width, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1
        toastLabel.layer.cornerRadius = 6;
        toastLabel.clipsToBounds  =  true
        let win:UIWindow = UIApplication.shared.delegate!.window!!
        win.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.5, options: .curveEaseInOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
        
        //MARK:- Facebook Login
    static func fbLogout () {
        let logoutManager = LoginManager()
        logoutManager.logOut()
    }
    static func fbLogin(vc: UIViewController, callback: @escaping (Bool) -> Void) {
               let loginManager = LoginManager()
               loginManager.logOut()
               loginManager.logIn(permissions:[ .publicProfile, .email, .userFriends ], viewController: vc) { loginResult in
                switch loginResult {
                   case .failed(let error):
                       print(error)
                       Utils.setLoginFb(login: false)
                       callback(false)
                   case .cancelled:
                       print("User cancelled login process.")
                       Utils.setLoginFb(login: false)
                       callback(false)
                   case .success( _, _, _):
                       print("Logged in!")
                       Utils.setLoginFb(login: true)
                       self.getFBUserData(callback: { reload in
                        callback(reload)
                       })
                   }
               }
           }
           
    static func getFBUserData(callback: @escaping (Bool) -> Void) {
               //which if my function to get facebook user details
               if((AccessToken.current) != nil){
                   
                   GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                       if (error == nil){
                           
                           let dict = result as! [String : AnyObject]
                           print(dict)
                           let picutreDic = dict as NSDictionary
                           let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                           let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                            let urlAvatar = tmpURL2.object(forKey: "url") as! String
                           
                           let nameOfUser = picutreDic.object(forKey: "name") as! String
    //                       self.lblUserName.text = nameOfUser
                           
                            var tmpEmailAdd = ""
                           if let emailAddress = picutreDic.object(forKey: "email") {
                               tmpEmailAdd = emailAddress as! String
    //                           self.lblUserEmail.text = tmpEmailAdd
                           }
                           else {
                               var usrName = nameOfUser
                               usrName = usrName.replacingOccurrences(of: " ", with: "")
                               tmpEmailAdd = usrName+"@facebook.com"
                           }
                        self.saveToCoreData(urlAvatar: urlAvatar, name: nameOfUser, birthDay: "", mail: tmpEmailAdd)
                        callback(true)
                           // PLEASE SUBSCRIBE MY CHANNEL IT WILL MOTIVATE ME KEEP UPLOAD VIDEOS ;)
                           
                          
                       }
                       
                       print(error?.localizedDescription as Any)
                   })
               }
           }
    
    static func saveToCoreData(urlAvatar: String, name: String, birthDay: String, mail: String ) {
        if #available(iOS 10.0, *) {
            let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
            let context = appDelegate.persistentContainer.viewContext
            let newEntry = ItemFacebook(context: context)
            newEntry.urlAvatar = urlAvatar
            newEntry.mail = mail
            newEntry.birthDay = birthDay
            newEntry.name = name
            appDelegate.saveContext()
        }
    }
    
    // check login logout fb
    static func setLoginFb(login: Bool) {
        UserDefaults.standard.set(login, forKey: "didLogin")
    }
    static func getLoginFb() -> Bool {
        return UserDefaults.standard.bool(forKey: "didLogin")
    }
    
    // delete CoreData
    static func deleteEntities(entity: String) {
        if #available(iOS 10.0, *) {
            let context = ( UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            do
            {
                try context.execute(deleteRequest)
                try context.save()
            }
            catch
            {
                print ("There was an error")
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
}
