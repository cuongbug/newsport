//
//  AppService.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import Alamofire

class AppService {
    static var urlHome: String = "https://thethao247.vn/trang-chu.rss"
    static var urlFootballWorld: String = "https://thethao247.vn/bong-da-quoc-te-c2.rss"
    static var urlNewsWorld: String = "https://thethao247.vn/bong-da-quoc-te-c2.rss"
    static var urlSportNews: String = "https://thethao247.vn/v-league-c15.rss"
    static var urlVideo: String = "https://thethao247.vn/video-bong-da-c80.rss"
    static var urlLiveScore: String = "https://thethao247.vn/bong-da-viet-nam-c1.rss"
    
    
    // moreDetail
    // section 1
    static var urlVleague247 = "https://thethao247.vn/v-league-c15.rss"
    static var urlNumberOne247 = "https://thethao247.vn/giai-hang-nhat-c16.rss"
    static var urlDoiTuyenQuocGia247 = "https://thethao247.vn/tuyen-quoc-gia-vn-c19.rss"
    static var urlBongDaNu247 = "https://thethao247.vn/bong-da-nu-viet-nam-c20.rss"
    static var urlSukienBinhLuan = "https://thethao247.vn/noi-soi-bong-da-viet-c33.rss"
    
    static var urlAFFCup = "https://thethao247.vn/aff-cup-2018-c92.rss"
    static var urlU17 = "https://thethao247.vn/u17-quoc-gia-c161.rss"
    static var urlAsiad = "https://thethao247.vn/asiad-c167.rss"
    static var urlU23ChauA = "https://thethao247.vn/u23-chau-a-2020-c281.rss"
    
    static var urlU19VN = "https://thethao247.vn/u19-quoc-gia-c225.rss"
    static var urlAFCCup = "https://thethao247.vn/afc-cup-champions-league-c226.rss"
    static var urlVideoU23 = "https://thethao247.vn/video-u23-chau-a-2020-c282.rss"
    static var urlVDQGHaLan = "https://thethao247.vn/giai-vdqg-ha-lan-2019-20-c279.rss"
    
    
    // section 2
    static var urlNamSeaGame = "https://thethao247.vn/bong-da-nam-sea-games-c129.rss"
    static var urlNuSeaGame = "https://thethao247.vn/bong-da-nu-sea-games-c130.rss"
    static var urlTinBenLe = "https://thethao247.vn/tin-ben-le-c131.rss"
    static var urlCacMonKhac = "https://thethao247.vn/cac-mon-khac-c132.rss"
    static var urlNhatKySeaGame = "https://thethao247.vn/nhat-ki-sea-games-c133.rss"
    static var urlVideoSeaGame = "https://thethao247.vn/video-sea-games-c134.rss"
    
    // section 3
    static var urlBongDaAnh = "https://thethao247.vn/bong-da-anh-c8.rss"
    static var urlCupFA = "https://thethao247.vn/cup-fa-c22.rss"
    static var urlNgoaiHangAnh = "https://thethao247.vn/ngoai-hang-anh-c23.rss"
    static var urlGiaiKhacAnh = "https://thethao247.vn/bong-da-anh-cac-giai-khac-c24.rss"
    static var urlCupLienDoanAnh = "https://thethao247.vn/cup-lien-doan-c58.rss"
    
    static var urlBongDaTBN = "https://thethao247.vn/bong-da-tbn-c9.rss"
    static var urlLaliga = "https://thethao247.vn/La-Liga-c59.rss"
    static var urlCupNhaVua = "https://thethao247.vn/cup-nha-vua-c60.rss"
    static var urlGiaiKhacTBN = "https://thethao247.vn/bong-da-TBN-cac-giai-khac-c61.rss"
    static var urlBongDaY = "https://thethao247.vn/bong-da-y-c10.rss"
    
    static var urlSeriaA = "https://thethao247.vn/serie-a-c62.rss"
    static var urlCoppaItalia = "https://thethao247.vn/coppa-italia-c63.rss"
    static var urlBongDaDuc = "https://thethao247.vn/bong-da-duc-c11.rss"
    static var urlBundesliga = "https://thethao247.vn/bundes-liga-c65.rss"
    
    static var urlCupQuocGiaDuc = "https://thethao247.vn/cup-quoc-gia-duc-c66.rss"
    static var urlBongDaPhap = "https://thethao247.vn/bong-da-phap-c12.rss"
    static var urlLigue1 = "https://thethao247.vn/ligue-one-c68.rss"

    static var urlCupQuocGiaPhap = "https://thethao247.vn/bong-da-Phap-cup-quoc-gia-c69.rss"
    static var urlGiaiKhacPhap = "https://thethao247.vn/bong-da-Phap-cup-quoc-gia-c69.rss"
    static var urlCupC1 = "https://thethao247.vn/champions-league-c13.rss"
    static var urlChungKetC1 = "https://thethao247.vn/chung-ket-cup-c1-c91.rss"
    
    // section 4
    static var urlVideoWorldCup = "https://thethao247.vn/world-cup-2014-c110.rss"
    static var urlVongLoaiWorldCup = "https://thethao247.vn/tin-tuc-world-cup-2014-c152.rss"
    static var urlLichThiDauWorldCup = "https://thethao247.vn/lich-thi-dau-ket-qua-bxh-c229.rss"
    static var urlTrucTiepWorldCup = "https://thethao247.vn/truc-tiep-c230.rss"
    
    static var urlDanhSachWorldCup = "https://thethao247.vn/danh-sach-doi-tuyen-c231.rss"
    static var urlTinHotWorldCup = "https://thethao247.vn/tin-hot-world-cup-c232.rss"
    static var urlNhanDinhWorldCup = "https://thethao247.vn/nhan-dinh-keo-c233.rss"
    
    // section 5
    static var urlChungKetEuro = "https://thethao247.vn/chung-ket-euro-c215.rss"
    static var urlEropa = "https://thethao247.vn/europa-league-c75.rss"
    
    // section 6
    static var urlLichThiDau = "https://thethao247.vn/lich-thi-dau-bong-da-c54.rss"
    static var urlKetQua = "https://thethao247.vn/ket-qua-bong-da-c55.rss"
    static var urlBangXepHang = "https://thethao247.vn/bang-xep-hang-bong-da-c56.rss"
    
    static func callApiXML(url: String, callback: @escaping ([Item]) -> Void) {
        var arrItem: [Item] = []
        Alamofire.request(url).response(completionHandler: { response in
            if let data = response.data {
                TIFeedParser.parseRSS(xmlData: data, onSuccess: { chanel  in
                    arrItem = chanel.items
                    callback(arrItem)
                }, onNotFound: {
                    callback(arrItem)
                }, onFailure: { error in
                    callback(arrItem)
                })
            }
        })
    }
    
    static func checkNetWork() -> Bool {
        if let isConected = NetworkReachabilityManager()?.isReachable {
            if isConected {
                Utils.setCheckNetWork(isConected: true)
                return true
            } else {
                Utils.setCheckNetWork(isConected: false)
                return false
            }
        }
        return false
    }
    
    
}
