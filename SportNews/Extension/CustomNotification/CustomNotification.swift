//
//  CustomNotification.swift
//  SportNews
//
//  Created by ManhCuong on 3/4/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class CustomNotification {
    static func registerNotification() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.alert, .badge, .sound], completionHandler: {(accepted, error) in
                if accepted {
                    print("true")
                    self.setStatusNotification(accepted: accepted)
                } else {
                    print("false")
                    self.setStatusNotification(accepted: accepted)
                }
            })
        } else {
            // Fallback on earlier versions
            let notificationSettings = UIUserNotificationSettings(types:[ .alert, .sound, .badge,], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        }
    }
    static func setStatusNotification(accepted: Bool){
            UserDefaults.standard.set(accepted, forKey: "statusNotifi")
    }
    static func getStatusNotification() -> Bool {
        let statusNoti: Bool = UserDefaults.standard.bool(forKey: "statusNotifi")
        return statusNoti
    }
    static func openSettingURL() {
        if let aString = URL(string: UIApplication.openSettingsURLString) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(aString, options: [:], completionHandler: { success in
                    //                SlideNavigationController.sharedInstance().closeMenu {
                    //
                    //                }
                })
            } else {
                // Fallback on earlier versions
                
            }
        }
    }
}
