//
//  Extension.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static func downloadFromRemoteURL(_ url: URL, completion: @escaping (UIImage?,Error?)->()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil, let image = UIImage(data: data) else {
                DispatchQueue.main.async{
                    completion(nil,error)
                }
                return
            }
            DispatchQueue.main.async() {
                completion(image,nil)
            }
            }.resume()
    }
    
    static func getImage(url: String, imageDetail: UIImageView) -> UIImageView {
        let url = URL(string: url)
        UIImage.downloadFromRemoteURL(url!, completion: { image, error in
            guard let image = image, error == nil else { print(error as Any);return }
            imageDetail.image = image
            imageDetail.contentMode = .scaleAspectFill
        })
        return imageDetail
    }
}

extension UIViewController: UIGestureRecognizerDelegate {
    public func addSwipLeft() {
        let edgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(userSwipedFromEdge))
        edgeGestureRecognizer.edges = UIRectEdge.left
        edgeGestureRecognizer.delegate = self
        self.view.addGestureRecognizer(edgeGestureRecognizer)
    }
    @objc func userSwipedFromEdge(sender: UIScreenEdgePanGestureRecognizer) {
        if sender.edges == UIRectEdge.left {
            if let navigation = self.navigationController {
                navigation.popViewController(animated: true)
                return
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// color app
extension UIColor {
    public class var mainColor: UIColor {
        if Utils.getCheckDarkMode() {
            return UIColor.init(hexString: Utils.getBaseColor()) }
        else {
            return UIColor.init(hexString: Utils.getColorApp())
        }
    }
    public class var darkColor: UIColor {
        return UIColor.init(hexString: Utils.getDarkModeColor())
    }
    public class var whiteColor: UIColor {
        return UIColor.init(hexString: Utils.getWhiteColor())
    }
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

// add font Base
extension UIFont {
    static func sfProTextRegular(size: CGFloat) -> UIFont {
        return UIFont.init(name: "SFProText-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    static func sfProTextBold(size: CGFloat) -> UIFont {
        return UIFont.init(name: "SFProText-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }
    static func sfProTextMedium(size: CGFloat) -> UIFont {
        return UIFont.init(name: "SFProText-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    static func sfProTextLight(size: CGFloat) -> UIFont {
        return UIFont.init(name: "SFProText-Light", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
extension NSObject {
    public func localizedString(key: String) -> String {
        var language = ""
        if Utils.getLanguage() {
            language = "vi"
        } else {
            language = "en"
        }
        let path = Bundle.main.path(forResource: language, ofType: "lproj")!
        let bundle = Bundle.init(path: path)
        let localizedString = bundle?.localizedString(forKey: key, value: "", table: "")
        return localizedString ?? ""
    }
}
