//
//  CustomHeaderView.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class CustomHeaderView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.mainColor
    }
    
}
