//
//  ScreenSize.swift
//  SportNews
//
//  Created by ManhCuong on 2/26/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

enum SizeScreen {
    case iphone5
    case iphone6
    case iphone7Plus
    case iphoneX
    case iphoneXMax
    case ipad
}
class ScreenSizeApp {
    static func checkDevice() -> SizeScreen {
        let heightScreenSize = UIScreen.main.bounds.height
        switch heightScreenSize {
        case 568:
            return .iphone5
        case 667:
            return .iphone6
        case 736:
            return .iphone7Plus
        case 812:
            return .iphoneX
        case 896:
            return .iphoneXMax
        default:
            return .ipad
        }
    }
    static func setContraintHeader(layoutContraint: NSLayoutConstraint) {
        if checkDevice() == .iphoneX {
            layoutContraint.constant = 70
        } else {
            layoutContraint.constant = 55
        }
    }
}
