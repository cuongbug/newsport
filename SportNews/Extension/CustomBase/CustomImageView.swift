//
//  CustomImageView.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class CustomImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 6 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
