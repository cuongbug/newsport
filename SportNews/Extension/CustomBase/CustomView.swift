//
//  CustomView.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class CustomView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 6 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var isShadow: Bool = true {
        didSet {
            self.clipsToBounds = !isShadow
        }
    }
    @IBInspectable var shadowColor: UIColor = UIColor.lightGray {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = shadowOffset
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 1 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
}
