//
//  DetailModel.swift
//  SportNews
//
//  Created by ManhCuong on 2/26/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation

class DetailModel {
    func gotoPresenter(linkUrl: String, title: String, callback: (String, String) -> Void) {
        callback(linkUrl, title)
    }
}
