//
//  DetailPresenter.swift
//  SportNews
//
//  Created by ManhCuong on 2/26/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class DetailPresenter {
    func binDataToModel(link: String, title: String, callback: (String, String) -> Void) {
        let model = DetailModel()
        model.gotoPresenter(linkUrl: link, title: title, callback: { linkUrl, titleText in
            callback(linkUrl, titleText)
        })
    }
}
