//
//  ShareTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/12/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class ShareTableViewCell: UITableViewCell {

    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        labelTitle.font = .sfProTextRegular(size: 14)
        labelTitle.textColor = .darkColor
        imageTitle.clipsToBounds = true
        imageTitle.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
