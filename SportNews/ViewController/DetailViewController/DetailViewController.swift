//
//  DetailViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/26/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var heightHeaderView: NSLayoutConstraint!
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var webViewDetail: UIWebView!
    @IBOutlet weak var labelLoading: UILabel!
    
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var headerView: CustomHeaderView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewShare: UIView!
    
    @IBOutlet weak var loaddingView: UIActivityIndicatorView!
    @IBOutlet weak var buttonSaveNew: UIButton!
    
    var arrTitleShare: [String] = []
    var arrImageTitle: [UIImage] = []
    
    var isShowShare: Bool = false
    var link: String = ""
    var titleText: String = ""
    var pubDate: String = ""
    var urlImage: String = ""
    var titleNews: String = ""
    override func viewWillAppear(_ animated: Bool) {
        headerView.backgroundColor = UIColor.mainColor
        self.loaddingView.startAnimating()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        arrTitleShare = ["Lưu tin", "Chia sẻ", "Coppy đường dẫn"]
        arrImageTitle = [UIImage.init(named: "ic_save_news")!,
                         UIImage.init(named: "ic_share_news")!,
                         UIImage.init(named: "ic_coppy_news")!]
        heightTableView.constant = CGFloat(arrTitleShare.count*30)
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeaderView)
        webViewDetail.delegate = self
        if link.count > 0 {
            self.webViewDetail.loadRequest(URLRequest(url: URL(string: self.link)!))
        }
            self.labelTitle.text = titleText
        self.addSwipLeft()
        self.viewShare.isHidden = true
        setDataTableView()
    }
    func setDataTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ShareTableViewCell", bundle: nil), forCellReuseIdentifier: "ShareTableViewCell")
        tableView.layer.cornerRadius = 4
        tableView.layer.borderColor = UIColor.mainColor.cgColor
        tableView.layer.borderWidth = 0.5
        tableView.isScrollEnabled = false
    }
    func binData(url: String, date: String, titleNews: String, title: String, urlImage: String) {
        self.link = url
        self.titleText = title
        self.pubDate = date
        self.urlImage = urlImage
        self.titleNews = titleNews
    }
    @IBAction func onBack(_ sender: UIButton) {
        guard let navigation = self.navigationController else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        navigation.popViewController(animated: true)
    }
    
    @IBAction func saveNews(_ sender: UIButton) {
        if !isShowShare {
            isShowShare = true
            self.viewShare.isHidden = false
        } else {
            isShowShare = false
            viewShare.isHidden = true
        }
    }
    private func saveDataToCore() {
        if #available(iOS 10.0, *) {
            let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
            let context = appDelegate.persistentContainer.viewContext
            let newEntry = ItemSave(context: context)
            newEntry.linkWebView = self.link
            newEntry.titleSave = self.titleNews
            newEntry.linkUrlSave = self.urlImage
            newEntry.pubDateSave = self.pubDate
            appDelegate.saveContext()
            Utils.showMessage(message: "Đã lưu!", font: .systemFont(ofSize: 16))
            self.buttonSaveNew.isHidden = true
        }
    }
    
    //MARK: - UIWebViewDelegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.viewLoading.clipsToBounds = true
        self.viewLoading.isHidden = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.labelLoading.text = error as? String
    }
}
extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitleShare.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShareTableViewCell", for: indexPath) as! ShareTableViewCell
        cell.labelTitle.text = arrTitleShare[indexPath.row]
        cell.imageTitle.image = arrImageTitle[indexPath.row]
        return cell
    }
}

