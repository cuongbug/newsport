//
//  HomePresenter.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class HomePresenter {
    let modelHome = ModelHome()
    func requestToModel(url: String, callback: @escaping ([Item], Bool) -> Void) {
        modelHome.setData(url: url, callback: {data in
            if AppService.checkNetWork() {
                callback(data, true)
            } else {
                callback([], false)
            }
        })
    }
    func gotoDetailVC(link: String, date: String, titleNews: String, title: String, urlImage: String, vc: UIViewController) {
        let detailVC = DetailViewController()
        detailVC.binData(url: link, date: date, titleNews: titleNews, title: title, urlImage: urlImage)
        vc.navigationController?.pushViewController(detailVC, animated: true)
//        vc.present(detailVC, animated: true, completion: nil)
    }
}
