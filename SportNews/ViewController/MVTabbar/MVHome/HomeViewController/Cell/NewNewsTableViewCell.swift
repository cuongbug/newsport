//
//  NewNewsTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class NewNewsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var labelDecription: UILabel!
    @IBOutlet weak var labelNews: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageDetail.layer.cornerRadius = 4
        labelNews.text = localizedString(key: "label_home_new_news_homevc")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arr: [Item], index: Int) {
        self.labelDecription.text = arr[index].title
        labelNews.textColor = .darkColor
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
    }
    func binDataNoNet(arr: [ItemNewsHome0], index: Int) {
        self.labelDecription.text = arr[index].title
        labelNews.textColor = .darkColor
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
    }
    
}
