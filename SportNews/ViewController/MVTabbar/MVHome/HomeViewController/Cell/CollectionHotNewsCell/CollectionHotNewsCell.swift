//
//  CollectionHotNewsCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class CollectionHotNewsCell: UICollectionViewCell {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var labelDecription: UILabel!
    @IBOutlet weak var shadowView: CustomView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageDetail.layer.cornerRadius =  6
        imageDetail.clipsToBounds = true
    }
    func binData(arr: [Item], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelDecription.text = arr[index].title
        setUI()
    }
    func binDataNoNet(arr: [ItemNewsHome01], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelDecription.text = arr[index].title
        setUI()
    }
    func setUI() {
        self.labelDecription.textColor = .darkColor
        shadowView.backgroundColor = .whiteColor
    }

}
