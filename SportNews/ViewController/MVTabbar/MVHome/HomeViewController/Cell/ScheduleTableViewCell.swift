//
//  ScheduleTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelHot: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelHot.text = localizedString(key: "label_hot_truss_homevc")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arr: [Item], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelTitle.text = arr[index].title
        self.labelHot.textColor = .darkColor
    }
    func binDataNoNet(arr: [ItemNewsHome03], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelTitle.text = arr[index].title
        self.labelHot.textColor = .darkColor
    }
}
