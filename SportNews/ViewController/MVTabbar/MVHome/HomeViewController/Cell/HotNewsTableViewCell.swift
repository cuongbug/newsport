//
//  HotNewsTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol HotNewsTableViewCellDelegate {
    func gotoDetail(link: String,date: String, titleNews: String, title: String, urlImage: String)
}

class HotNewsTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var arrNewsWorld: [Item] = []
    var arrNewWorldNoNet: [ItemNewsHome01] = []
    var index: Int?
    var mvdelegate: HotNewsTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setDataCollectionView()
    }
    func setDataCollectionView() {
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "CollectionHotNewsCell", bundle: nil), forCellWithReuseIdentifier: "CollectionHotNewsCell")
    }
    func binData(arr: [Item], index: Int) {
        self.arrNewsWorld = arr
        self.index = index
        collectionView.reloadData()
    }
    func binDataNoNet(arr: [ItemNewsHome01], index: Int) {
        self.arrNewWorldNoNet = arr
        self.index = index
    }
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (UIScreen.main.bounds.width - 48) / 2
        let height: CGFloat = UIScreen.main.bounds.width / 2
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionHotNewsCell", for: indexPath) as! CollectionHotNewsCell
        if arrNewsWorld.count > 0 {
            cell.binData(arr: arrNewsWorld, index: indexPath.row)
        }
        if !Utils.getCheckNetWork() {
            cell.binDataNoNet(arr: arrNewWorldNoNet, index: indexPath.row)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        mvdelegate?.gotoDetail(link: arrNewsWorld[indexPath.row].link!, date: arrNewsWorld[indexPath.row].pubDate!, titleNews: arrNewsWorld[indexPath.row].title!, title: localizedString(key: "label_home_new_news_homevc"), urlImage: arrNewsWorld[indexPath.row].urlImage!)
    }
}
