//
//  CellHotNews1TableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class CellHotNews1TableViewCell: UITableViewCell {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // cell Home
    func binData(arr: [Item], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelDate.text = arr[index].pubDate
        self.labelTitle.text = arr[index].title
        setDarkModeUI()
        
    }
    func binDataNoNet(arr: [ItemNewsHome02], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelDate.text = arr[index].pubDate
        self.labelTitle.text = arr[index].title
        setDarkModeUI()
    }
    
    // cell Setting
    func binDataToItemSave(arr: [ItemSave], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].linkUrlSave!, imageDetail: self.imageDetail)
        self.labelTitle.text = arr[index].titleSave
        self.labelDate.text = arr[index].pubDateSave
        setDarkModeUI()
    }
    func setDarkModeUI() {
        self.labelDate.textColor = .darkColor
        self.labelTitle.textColor = .darkColor
    }
    
}
