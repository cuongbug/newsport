//
//  HotNews1TableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol HotNews1TableViewCellDelegate {
    func showDetail(link: String, date: String, titleNews: String, title: String, urlImage: String)
}
class HotNews1TableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var labelHotNews: UILabel!
    var mvdelegate: HotNews1TableViewCellDelegate?
    var arrDataNewsSport: [Item] = []
    var arrDataNewsSportsNoNet: [ItemNewsHome02] = []
    var index: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelHotNews.text = localizedString(key: "label_home_hot_news_homevc")
        setDataTableView()
        tableView.isScrollEnabled = false
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "CellHotNews1TableViewCell", bundle: nil), forCellReuseIdentifier: "CellHotNews1TableViewCell")
    }
    func binData(arr: [Item], index: Int) {
        labelHotNews.textColor = .darkColor
        self.arrDataNewsSport = arr
        self.index = index
        self.tableView.reloadData()
    }
    func binDataNoNet(arr: [ItemNewsHome02], index: Int) {
        labelHotNews.textColor = .darkColor
        self.arrDataNewsSportsNoNet = arr
        self.index = index
        self.tableView.reloadData()
    }
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.height - 37) / 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHotNews1TableViewCell", for: indexPath) as! CellHotNews1TableViewCell
        if arrDataNewsSport.count > 0 {
            cell.binData(arr: arrDataNewsSport, index: indexPath.row)
        }
        if !Utils.getCheckNetWork() {
            cell.binDataNoNet(arr: arrDataNewsSportsNoNet, index: indexPath.row)
        }
        return cell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mvdelegate?.showDetail(link: arrDataNewsSport[indexPath.row].link!, date: arrDataNewsSport[indexPath.row].pubDate!, titleNews: arrDataNewsSport[indexPath.row].title!, title: localizedString(key: "label_home_hot_news_homevc"), urlImage: arrDataNewsSport[indexPath.row].urlImage!)
        tableView.reloadData()
    }

    
}
