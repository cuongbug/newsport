//
//  HomeViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 10.0, *)
class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let refreshControl = UIRefreshControl()
    let interactor = Interactor()
    var arrData: [Item] = []
    var arrDataNoNetWork: [ItemNewsHome0] = []
    var arrDataNewsWorld: [Item] = []
    var arrDataNewsWorldNoW: [ItemNewsHome01] = []
    var arrDataNewsSport: [Item] = []
    var arrDataNewsSportNoNet: [ItemNewsHome02] = []
    var arrMatchHot: [Item] = []
    var arrMatchHotNoNet: [ItemNewsHome03] = []
    let presenter = HomePresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.headerView.backgroundColor = UIColor.mainColor
        self.tableView.backgroundColor = .whiteColor
        self.labelTitle.text = localizedString(key: "title_home_homevc")
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeader)
        setDataTableView()
        setParamsRefresh()
        changeDarkMode()
        addRefresh()
    }
    func setParamsRefresh() {
        setParams()
        setParamsNewsWorld()
        setParamsNewsSport()
        setParamsMatchHot()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.headerView.backgroundColor = UIColor.mainColor
        self.tableView.backgroundColor = .whiteColor
        self.labelTitle.text = localizedString(key: "title_home_homevc")
        addRefresh()
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NewNewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewNewsTableViewCell")
        tableView.register(UINib(nibName: "HotNewsTableViewCell", bundle: nil), forCellReuseIdentifier: "HotNewsTableViewCell")
        tableView.register(UINib(nibName: "HotNews1TableViewCell", bundle: nil), forCellReuseIdentifier: "HotNews1TableViewCell")
        tableView.register(UINib(nibName: "ScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "ScheduleTableViewCell")
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height / 2.5
        } else if indexPath.row == 1 {
            return (3*UIScreen.main.bounds.width + 96) / 2
        } else if indexPath.row == 2 {
            return UIScreen.main.bounds.height
        } else {
            return UIScreen.main.bounds.height / 3
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewNewsTableViewCell", for: indexPath) as! NewNewsTableViewCell
            if arrData.count > 0 {
                cell.binData(arr: arrData, index: 0)
            }
            if !AppService.checkNetWork() {
                cell.binDataNoNet(arr: arrDataNoNetWork, index: 0)
            }
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HotNewsTableViewCell", for: indexPath) as! HotNewsTableViewCell
            cell.mvdelegate = self
            if arrDataNewsWorld.count > 0 {
                cell.binData(arr: arrDataNewsWorld, index: indexPath.row)
            }
            if !AppService.checkNetWork() {
                cell.binDataNoNet(arr: arrDataNewsWorldNoW, index: indexPath.row)
            }
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HotNews1TableViewCell", for: indexPath) as! HotNews1TableViewCell
            cell.mvdelegate = self
            if arrDataNewsSport.count > 0 {
                cell.binData(arr: arrDataNewsSport, index: indexPath.row)
            }
            if !AppService.checkNetWork() {
                cell.binDataNoNet(arr: arrDataNewsSportNoNet, index: indexPath.row)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath) as! ScheduleTableViewCell
            if arrMatchHot.count > 0 {
                cell.binData(arr: arrMatchHot, index: indexPath.row)
            }
            if !AppService.checkNetWork() {
                cell.binDataNoNet(arr: arrMatchHotNoNet, index: indexPath.row)
            }
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            showDetailVC(url: arrData[indexPath.row].link!, date:  arrData[indexPath.row].pubDate!, titleNews: arrData[indexPath.row].title!, title: localizedString(key: "label_home_new_news_homevc"), urlImage: arrData[indexPath.row].urlImage!)
        } else if indexPath.row == 3 {
            showDetailVC(url: arrMatchHot[indexPath.row].link!, date: arrMatchHot[indexPath.row].pubDate!, titleNews: arrMatchHot[indexPath.row].title!, title: localizedString(key: "label_hot_truss_homevc"), urlImage: arrMatchHot[indexPath.row].urlImage!)
        }
    }
    // MARK: - Presenter
    
    // section 0
    func setParams() {
        DispatchQueue.global(qos: .userInteractive).async {
            let urlHome = AppService.urlHome
            self.presenter.requestToModel(url: urlHome, callback: { data, isColect  in
                if isColect {
                    self.arrData = data
                    Utils.deleteEntities(entity: "ItemNewsHome0")
                    self.saveCoreHome0(arrFromService: data)
                    self.tableView.reloadData()
                } else {
                    self.arrDataNoNetWork = self.fetchDataHome0()
                    self.tableView.reloadData()
                }
            })
        }
    }
    func saveCoreHome0(arrFromService: [Item]) {
        for var i in 0..<arrFromService.count {
            let newEntry = ItemNewsHome0(context: context)
            newEntry.title = arrFromService[i].title
            newEntry.link = arrFromService[i].link
            newEntry.urlImage = arrFromService[i].urlImage
            newEntry.pubDate = arrFromService[i].pubDate
            newEntry.desCription = arrFromService[i].description
            newEntry.contentEncoded = arrFromService[i].contentEncoded
            newEntry.thumbnail = arrFromService[i].thumbnail
            appDelegate.saveContext()
            i = i + 1
        }
    }
    func fetchDataHome0() -> [ItemNewsHome0] {
        var itemHome: [ItemNewsHome0] = []
        do {
            itemHome = try
            context.fetch(ItemNewsHome0.fetchRequest())
            print(itemHome.count)
        } catch {
            print("Couldn't Fetch Data")
        }
        return itemHome
    }
    
    // section 1
    func setParamsNewsWorld() {
        DispatchQueue.global(qos: .userInteractive).async {
            let urlNewsWorld = AppService.urlFootballWorld
            self.presenter.requestToModel(url: urlNewsWorld, callback: {data, isColect  in
                    if isColect {
                        Utils.deleteEntities(entity: "ItemNewsHome01")
                        self.arrDataNewsWorld = data
                        self.tableView.reloadData()
                    } else {
                        self.arrDataNewsWorldNoW = self.fetchDataHome1()
                        self.tableView.reloadData()
                    }
                })
            }
        }
    func saveCoreHome1(arrFromService: [Item]) {
        for var i in 0..<arrFromService.count {
            let newEntry = ItemNewsHome01(context: context)
            newEntry.title = arrFromService[i].title
            newEntry.link = arrFromService[i].link
            newEntry.urlImage = arrFromService[i].urlImage
            newEntry.pubDate = arrFromService[i].pubDate
            newEntry.desCription = arrFromService[i].description
            newEntry.contentEncoded = arrFromService[i].contentEncoded
            newEntry.thumbnail = arrFromService[i].thumbnail
            appDelegate.saveContext()
            i = i + 1
        }
    }
    func fetchDataHome1() -> [ItemNewsHome01] {
            var itemHome: [ItemNewsHome01] = []
            do {
                itemHome = try
                context.fetch(ItemNewsHome01.fetchRequest())
                print(itemHome.count)
            } catch {
                print("Couldn't Fetch Data")
            }
            return itemHome
        }
    
    
    // section 2
    func setParamsNewsSport() {
        DispatchQueue.global(qos: .utility).async {
            let urlNewsSport = AppService.urlSportNews
            self.presenter.requestToModel(url: urlNewsSport, callback: {data, isColect  in
                if isColect {
                    self.arrDataNewsSport = data
                    Utils.deleteEntities(entity: "ItemNewsHome02")
                    self.saveCoreHome2(arrFromService: data)
                    self.tableView.reloadData()
                } else {
                    self.arrDataNewsSportNoNet = self.fetchDataHome2()
                    self.tableView.reloadData()
                }
            })
        }
    }
    func saveCoreHome2(arrFromService: [Item]) {
        for var i in 0..<arrFromService.count {
            let newEntry = ItemNewsHome02(context: context)
            newEntry.title = arrFromService[i].title
            newEntry.link = arrFromService[i].link
            newEntry.urlImage = arrFromService[i].urlImage
            newEntry.pubDate = arrFromService[i].pubDate
            newEntry.desCription = arrFromService[i].description
            newEntry.contentEncoded = arrFromService[i].contentEncoded
            newEntry.thumbnail = arrFromService[i].thumbnail
            appDelegate.saveContext()
            i = i + 1
        }
    }
    func fetchDataHome2() -> [ItemNewsHome02] {
            var itemHome: [ItemNewsHome02] = []
            do {
                itemHome = try
                context.fetch(ItemNewsHome02.fetchRequest())
                print(itemHome.count)
            } catch {
                print("Couldn't Fetch Data")
            }
            return itemHome
        }
    
    // section 3
    func setParamsMatchHot() {
        DispatchQueue.global(qos: .utility).async {
            let urlMatchHot = AppService.urlLiveScore
            self.presenter.requestToModel(url: urlMatchHot, callback: {data, isColect  in
                if isColect {
                    Utils.deleteEntities(entity: "ItemNewsHome03")
                    self.saveCoreHome3(arrFromService: data)
                    self.arrMatchHot = data
                    self.tableView.reloadData()
                } else {
                    self.arrMatchHotNoNet = self.fetchDataHome3()
                    self.tableView.reloadData()
                }
            })
        }
    }
    func saveCoreHome3(arrFromService: [Item]) {
        for var i in 0..<arrFromService.count {
            let newEntry = ItemNewsHome03(context: context)
            newEntry.title = arrFromService[i].title
            newEntry.link = arrFromService[i].link
            newEntry.urlImage = arrFromService[i].urlImage
            newEntry.pubDate = arrFromService[i].pubDate
            newEntry.desCription = arrFromService[i].description
            newEntry.contentEncoded = arrFromService[i].contentEncoded
            newEntry.thumbnail = arrFromService[i].thumbnail
            appDelegate.saveContext()
            i = i + 1
        }
    }
    func fetchDataHome3() -> [ItemNewsHome03] {
            var itemHome: [ItemNewsHome03] = []
            do {
                itemHome = try
                context.fetch(ItemNewsHome03.fetchRequest())
                print(itemHome.count)
            } catch {
                print("Couldn't Fetch Data")
            }
            return itemHome
        }
    
    
    private func changeDarkMode() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDarkMode), name: NSNotification.Name("updateDarkMode"), object: nil)
    }
    @objc func updateDarkMode() {
        self.tableView.reloadData()
    }
    //MARK: - ShowMenu Trái
    @IBAction func showMenu(_ sender: UIButton) {
        performSegue(withIdentifier: "openMenu", sender: nil)
    }
    @IBAction func edgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .right)
        
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: interactor){
                self.performSegue(withIdentifier: "openMenu", sender: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = interactor
            destinationViewController.mvdelegate = self
        }
    }
}
@available(iOS 10.0, *)
extension HomeViewController: UIViewControllerTransitioningDelegate, MenuViewControllerDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    func selectedCell(vc: MoreDetailViewController, title: String) {
        vc.binDataTitle(title: title)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
    
@available(iOS 10.0, *)
extension HomeViewController: HotNewsTableViewCellDelegate, HotNews1TableViewCellDelegate {
    func showDetailVC(url: String, date: String, titleNews: String, title: String, urlImage: String) {
        let detailVC = DetailViewController()
        detailVC.binData(url: url, date: date, titleNews: titleNews, title: title, urlImage: urlImage)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    // MARK: - HotNewsTableViewCellDelegate
    func gotoDetail(link: String, date: String , titleNews: String, title: String, urlImage: String) {
        showDetailVC(url: link, date: date, titleNews: titleNews, title: title, urlImage: urlImage)
    }
    // MARK: - HotNews1TableViewCellDelegate
    func showDetail(link: String, date: String, titleNews: String, title: String, urlImage: String) {
        showDetailVC(url: link, date: date, titleNews: titleNews, title: title, urlImage: urlImage)
    }
    // refreshControl
    func addRefresh() {
        refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkColor])
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.darkColor

        refreshControl.addTarget(self, action: #selector(methodPullToRefresh(sender:)), for: UIControl.Event.valueChanged)

        self.tableView.addSubview(refreshControl)

    }

    @objc func methodPullToRefresh(sender: AnyObject)
    {
        setParamsRefresh()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.refreshControl.endRefreshing()
        })

    }
}
