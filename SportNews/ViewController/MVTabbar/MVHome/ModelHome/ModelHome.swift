//
//  ModelHome.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
class ModelHome {
    func setData(url: String, callback: @escaping ([Item]) -> Void) {
        AppService.callApiXML(url: url, callback: { data in
            callback(data)
        })
    }
}
