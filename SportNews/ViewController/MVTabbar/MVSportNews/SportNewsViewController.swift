//
//  SportNewsViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class SportNewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    
    @IBOutlet weak var headerView: CustomHeaderView!
    
    @IBOutlet weak var labelNewsFootball: UILabel!
    let interactor = Interactor()
    let refreshControl = UIRefreshControl()
    let presenter = HomePresenter()
    var arrNews: [Item] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeader)
        self.navigationController?.isNavigationBarHidden = true
        setDataTableView()
        setParams()
        changeDarkMode()
    }
    override func viewWillAppear(_ animated: Bool) {
        labelNewsFootball.text = localizedString(key: "title_sport_news_sportvc")
        headerView.backgroundColor = UIColor.mainColor
        self.view.backgroundColor = .whiteColor
        tableView.backgroundColor = .whiteColor
        addRefresh()
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "CellHotNews1TableViewCell", bundle: nil), forCellReuseIdentifier: "CellHotNews1TableViewCell")
        tableView.register(UINib(nibName: "SportNewsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "SportNewsHeaderTableViewCell")
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrNews.count > 0 {
            return arrNews.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UIScreen.main.bounds.height / 3
        } else {
            return UIScreen.main.bounds.height / 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SportNewsHeaderTableViewCell", for: indexPath) as! SportNewsHeaderTableViewCell
            cell.mvdelegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHotNews1TableViewCell", for: indexPath) as! CellHotNews1TableViewCell
            if arrNews.count > 0 {
                cell.binData(arr: arrNews, index: indexPath.row)
            }
            return cell
        }
    }
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            presenter.gotoDetailVC(link: arrNews[indexPath.row].link!, date: arrNews[indexPath.row].pubDate!, titleNews: arrNews[indexPath.row].title!, title: localizedString(key: "label_news_detail_vc"), urlImage: arrNews[indexPath.row].urlImage!, vc: self)
        }
        tableView.reloadData()
    }
    // MARK: - Presenter
    func setParams() {
        presenter.requestToModel(url: AppService.urlLiveScore, callback: {data, isColect  in
            self.arrNews = data
            self.tableView.reloadData()
            })
    }
    private func changeDarkMode() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDarkMode), name: NSNotification.Name("updateDarkMode"), object: nil)
    }
    @objc func updateDarkMode() {
        self.tableView.reloadData()
    }
    // refreshControl
      func addRefresh() {
          refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkColor])
          refreshControl.backgroundColor = UIColor.clear
          refreshControl.tintColor = UIColor.darkColor

          refreshControl.addTarget(self, action: #selector(methodPullToRefresh(sender:)), for: UIControl.Event.valueChanged)

          self.tableView.addSubview(refreshControl)

      }

      @objc func methodPullToRefresh(sender: AnyObject)
      {
          setParams()
          DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
              self.refreshControl.endRefreshing()
          })

      }
    
    // MARK: - ACtion
    
    @IBAction func showMenu(_ sender: UIButton) {
        performSegue(withIdentifier: "openMenuFromNews", sender: nil)
    }
    
    @IBAction func screenEdgePan(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .right)
        
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: Interactor()){
                self.performSegue(withIdentifier: "openMenuFromNews", sender: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = Interactor()
            destinationViewController.mvdelegate = self
        }
    }
    
}
extension SportNewsViewController: UIViewControllerTransitioningDelegate, MenuViewControllerDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    func selectedCell(vc: MoreDetailViewController, title: String) {
        vc.binDataTitle(title: title)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension SportNewsViewController: SportNewsHeaderTableViewCellDelegate {
    //MARK: - SportNewsHeaderTableViewCellDelegate
    func showDetailVC(url: String, date: String, titleNews: String, title: String, urlImage: String) {
        presenter.gotoDetailVC(link: url, date: date, titleNews: titleNews, title: title, urlImage: urlImage, vc: self)
    }
    
    
}
