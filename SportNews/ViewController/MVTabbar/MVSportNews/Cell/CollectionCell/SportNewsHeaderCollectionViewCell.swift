//
//  SportNewsHeaderCollectionViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class SportNewsHeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageDetailView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func binData(arr: [Item], index: Int) {
        self.imageDetailView = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetailView)
        self.labelTitle.text = arr[index].title
    }

}
