//
//  SportNewsHeaderTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol SportNewsHeaderTableViewCellDelegate {
    func showDetailVC(url: String, date: String, titleNews: String, title: String, urlImage: String)
}

class SportNewsHeaderTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let presenter = HomePresenter()
    var arrHeaderNews: [Item] = []
    var mvdelegate: SportNewsHeaderTableViewCellDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setDataCollectionView()
        setParamsNewsHeader()
    }
    func setDataCollectionView() {
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "SportNewsHeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SportNewsHeaderCollectionViewCell")
    }
    //MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrHeaderNews.count > 0 {
            return arrHeaderNews.count
        }
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 32, height: collectionView.frame.height - 16)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportNewsHeaderCollectionViewCell", for: indexPath) as! SportNewsHeaderCollectionViewCell
        if arrHeaderNews.count > 0 {
            cell.binData(arr: arrHeaderNews, index: indexPath.row)
            autoScrollCollectionView(indexPath: indexPath)
        }
        return cell
    }
    func autoScrollCollectionView(indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        })
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        mvdelegate?.showDetailVC(url: arrHeaderNews[indexPath.row].link!, date: arrHeaderNews[indexPath.row].pubDate!, titleNews: arrHeaderNews[indexPath.row].title!, title: localizedString(key: "label_news_detail_vc"), urlImage: arrHeaderNews[indexPath.row].urlImage!)
    }
    // MARK: - Presenter
    func setParamsNewsHeader() {
        presenter.requestToModel(url: AppService.urlSportNews, callback: {data, isColect  in
            self.arrHeaderNews = data
            self.collectionView.reloadData()
        })
    }
}
