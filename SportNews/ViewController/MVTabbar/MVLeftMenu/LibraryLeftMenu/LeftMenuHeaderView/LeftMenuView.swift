//
//  LeftMenuView.swift
//  SportNews
//
//  Created by ManhCuong on 2/26/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuView: UIView {
    
    @IBOutlet weak var textTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textTitle.textColor = .darkColor
        self.backgroundColor = .whiteColor
        // Initialization code
    }
}
