//
//  MenuViewController.swift
//  InteractiveSlideoutMenu
//
//  Created by Robert Chen on 2/7/16.
//
//  Copyright (c) 2016 Thorn Technologies LLC
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit
enum menuItemInSection {
    case TinWorldCup
    case LichThiDau
    case BenLeSanCo
    case AnhDep
    case VideoClip
    case BanDoc
    case HoSoDoiBong
    case LichSu
    case BXH
    case VLeague
    case HangNhat
    case CacDTQuocGia
    case CupQuoc
    case GiaiTre
    case CacGiaiKhac
    case BongDaNu
    case VFF
    case TinKhac
    case TinChuyenNhuong
    case BongDaAnh
    case BongDaTBN
    case BongDaDuc
    case BongDaY
    case BongDaPhap
    case BongDaChauMy
    case BongDaChauAu
    case ChampionsLeague
    case BongDaChauA
    case BongDaChauPhi
    case FIFA
    case GiaoHuu
    case NgoiSao
    case NhungTranHapDan
    case TongHopTranDau
    case HaiHuoc
    case TinSeaGame
    case BangTongSap
}
protocol MenuViewControllerDelegate {
    func selectedCell(vc: MoreDetailViewController, title: String)
}


class MenuViewController : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var mvdelegate: MenuViewControllerDelegate?
    var interactor:Interactor? = nil
    
    let menuSection = ["Trang Chủ", "Bóng Đá Việt Nam", "SEA Games 30", "Bóng Đá Quốc Tế", "World Cup 2022", "Euro 2020", "LiveScore"]
    let menuItemsSection0: [String] = []
    let menuItemsSection1 = ["V-League", "Hạng Nhất Việt Nam", "Đội tuyển Quốc gia", "Bóng Đá Nữ", "Sự kiện - Bình luận bóng đá Việt", "AFF Cup 2018", "U17 Quốc gia", "ASIAD", "U23 Châu Á", "U19 Việt Nam", "AFC Cup - Champions League", "Video U23 Châu Á 2020", "Giải VĐQG Hà Lan"]
    let menuItemsSection2 = ["Bóng đá Nam SEA Games", "Bóng đá nữ SEA Games", "Tin bên lề", "Các môn khác", "Nhật ký SEA Games 29", "Video"]
    let menuItemsSection3 = ["Bóng đá Anh", "Cúp FA", "Ngoại hạng Anh", "Các giải khác - Anh", "Cúp Liên đoàn Anh", "Bóng đá Tây Ban Nha", "La Liga", "Cúp Nhà Vua", "Các giải khác - Tây Ban Nha", "Bóng đá Ý", "Serie A", "Coppa Italia", "Bóng đá Đức", "Bundesliga", "Cúp Quốc gia Đức", "Bóng đá Pháp", "Ligue 1", "Cúp Quốc gia Pháp", "Các giải khác - Pháp", "Cúp C1", "Chung kết Cúp C1 2019"]
    let menuItemsSection4 = ["Video bàn thắng World Cup", "Vòng loại World Cup", "Lịch thi đấu - Kết quả - BXH", "Trực tiếp", "Danh sách đội tuyển", "Tin HOT World Cup", "Nhận định"]
    let menuItemsSection5 = ["Chung kết Euro 2020", "Europa League"]
    let menuItemsSection6 = ["Lịch thi đấu bóng đá", "Kết quả bóng đá", "Bảng xếp hạng bóng đá"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .whiteColor
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "LeftMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftMenuTableViewCell")
    }
    @IBAction func handleGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)

        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .left)
        
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: interactor){
                self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeMenu(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func delay(seconds: Double, completion:@escaping ()->()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        dismiss(animated: true){
            self.delay(seconds: 0.5){
//                self.menuActionDelegate?.reopenMenu()
            }
        }
    }
    
}

extension MenuViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuSection.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return menuItemsSection0.count
        } else if section == 1 {
            return menuItemsSection1.count
        } else if section == 2 {
            return menuItemsSection2.count
        } else if section == 3 {
            return menuItemsSection3.count
        } else if section == 4 {
            return menuItemsSection4.count
        } else if section == 5 {
            return menuItemsSection5.count
        } else {
            return menuItemsSection6.count
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader: LeftMenuView = Bundle.main.loadNibNamed("LeftMenuView", owner: self, options: nil)?.first as! LeftMenuView
        viewHeader.textTitle.text = menuSection[section]
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell") as! LeftMenuTableViewCell
        switch indexPath.section {
        case 0:
            print(indexPath.section)
        case 1:
            cell.binData(arr: menuItemsSection1, index: indexPath.row)
        case 2:
            cell.binData(arr: menuItemsSection2, index: indexPath.row)
        case 3:
            cell.binData(arr: menuItemsSection3, index: indexPath.row)
        case 4:
            cell.binData(arr: menuItemsSection4, index: indexPath.row)
        case 5:
            cell.binData(arr: menuItemsSection5, index: indexPath.row)
        case 6:
            cell.binData(arr: menuItemsSection6, index: indexPath.row)
        default:
            print(indexPath.row)
        }
        return cell
    }
}

extension MenuViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        self.didSelectCell(indexRow: indexPath.row, indexSection: indexPath.section)
        didSelectCell(indexRow: indexPath.row, indexSection: indexPath.section)
    }
    func didSelectCell(indexRow: Int, indexSection: Int) {
        if indexSection == 1 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlVleague247, title: menuItemsSection1[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlNumberOne247, title: menuItemsSection1[indexRow])
            case 2:
                self.getDataFromURL(url: AppService.urlDoiTuyenQuocGia247, title: menuItemsSection1[indexRow])
            case 3:
                self.getDataFromURL(url: AppService.urlBongDaNu247, title: menuItemsSection1[indexRow])
            case 4:
                self.getDataFromURL(url: AppService.urlSukienBinhLuan, title: menuItemsSection1[indexRow])
            case 5:
                self.getDataFromURL(url: AppService.urlAFFCup, title: menuItemsSection1[indexRow])
            case 6:
                self.getDataFromURL(url: AppService.urlU17, title: menuItemsSection1[indexRow])
            case 7:
                self.getDataFromURL(url: AppService.urlAsiad, title: menuItemsSection1[indexRow])
            case 8:
                self.getDataFromURL(url: AppService.urlU23ChauA, title: menuItemsSection1[indexRow])
            case 9:
                self.getDataFromURL(url: AppService.urlU19VN, title: menuItemsSection1[indexRow])
            case 10:
                self.getDataFromURL(url: AppService.urlAFCCup, title: menuItemsSection1[indexRow])
            case 11:
                self.getDataFromURL(url: AppService.urlVideoU23, title: menuItemsSection1[indexRow])
            case 12:
                self.getDataFromURL(url: AppService.urlVDQGHaLan, title: menuItemsSection1[indexRow])
            default:
                print(indexRow)
            }
        } else if indexSection == 2 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlNamSeaGame, title: menuItemsSection2[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlNuSeaGame, title: menuItemsSection2[indexRow])
            case 2:
                self.getDataFromURL(url: AppService.urlTinBenLe, title: menuItemsSection2[indexRow])
            case 3:
                self.getDataFromURL(url: AppService.urlCacMonKhac, title: menuItemsSection2[indexRow])
            case 4:
                self.getDataFromURL(url: AppService.urlNhatKySeaGame, title: menuItemsSection2[indexRow])
            case 5:
                self.getDataFromURL(url: AppService.urlVideoSeaGame, title: menuItemsSection2[indexRow])
            default:
                print(indexRow)
            }
        } else if indexSection == 3 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlBongDaAnh, title: menuItemsSection3[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlCupFA, title: menuItemsSection3[indexRow])
            case 2:
                self.getDataFromURL(url: AppService.urlNgoaiHangAnh, title: menuItemsSection3[indexRow])
            case 3:
                self.getDataFromURL(url: AppService.urlGiaiKhacAnh, title: menuItemsSection3[indexRow])
            case 4:
                self.getDataFromURL(url: AppService.urlCupLienDoanAnh, title: menuItemsSection3[indexRow])
            case 5:
                self.getDataFromURL(url: AppService.urlBongDaTBN, title: menuItemsSection3[indexRow])
            case 6:
                self.getDataFromURL(url: AppService.urlLaliga, title: menuItemsSection3[indexRow])
            case 7:
                self.getDataFromURL(url: AppService.urlCupNhaVua, title: menuItemsSection3[indexRow])
            case 8:
                self.getDataFromURL(url: AppService.urlGiaiKhacTBN, title: menuItemsSection3[indexRow])
            case 9:
                self.getDataFromURL(url: AppService.urlBongDaY, title: menuItemsSection3[indexRow])
            case 10:
                self.getDataFromURL(url: AppService.urlSeriaA, title: menuItemsSection3[indexRow])
            case 11:
                self.getDataFromURL(url: AppService.urlCoppaItalia, title: menuItemsSection3[indexRow])
            case 12:
                self.getDataFromURL(url: AppService.urlBongDaDuc, title: menuItemsSection3[indexRow])
            case 13:
                self.getDataFromURL(url: AppService.urlBundesliga, title: menuItemsSection3[indexRow])
            case 14:
                self.getDataFromURL(url: AppService.urlCupQuocGiaDuc, title: menuItemsSection3[indexRow])
            case 15:
                self.getDataFromURL(url: AppService.urlBongDaPhap, title: menuItemsSection3[indexRow])
            case 16:
                self.getDataFromURL(url: AppService.urlLigue1, title: menuItemsSection3[indexRow])
            case 17:
                self.getDataFromURL(url: AppService.urlCupQuocGiaPhap, title: menuItemsSection3[indexRow])
            case 18:
                self.getDataFromURL(url: AppService.urlGiaiKhacPhap, title: menuItemsSection3[indexRow])
            case 19:
                self.getDataFromURL(url: AppService.urlCupC1, title: menuItemsSection3[indexRow])
            case 20:
            self.getDataFromURL(url: AppService.urlChungKetC1, title: menuItemsSection3[indexRow])
                
            default:
                print(indexRow)
            }
        } else if indexSection == 4 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlVideoWorldCup, title: menuItemsSection4[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlVongLoaiWorldCup, title: menuItemsSection4[indexRow])
            case 2:
                self.getDataFromURL(url: AppService.urlLichThiDauWorldCup, title: menuItemsSection4[indexRow])
            case 3:
                self.getDataFromURL(url: AppService.urlTrucTiepWorldCup, title: menuItemsSection4[indexRow])
            case 4:
                self.getDataFromURL(url: AppService.urlDanhSachWorldCup, title: menuItemsSection4[indexRow])
            case 5:
                self.getDataFromURL(url: AppService.urlTinHotWorldCup, title: menuItemsSection4[indexRow])
            case 6:
                self.getDataFromURL(url: AppService.urlNhanDinhWorldCup, title: menuItemsSection4[indexRow])
            default:
                print(indexRow)
            }
        } else if indexSection == 5 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlChungKetEuro, title: menuItemsSection5[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlEropa, title: menuItemsSection5[indexRow])
            default:
                print(indexRow)
            }
        } else if indexSection == 6 {
            switch indexRow {
            case 0:
                self.getDataFromURL(url: AppService.urlLichThiDau, title: menuItemsSection6[indexRow])
            case 1:
                self.getDataFromURL(url: AppService.urlKetQua, title: menuItemsSection6[indexRow])
            case 2:
                self.getDataFromURL(url: AppService.urlBangXepHang, title: menuItemsSection6[indexRow])
            default:
                print(indexRow)
            }
        } else {
            print(indexSection)
        }
    }
    func getDataFromURL(url: String, title: String) {
        self.dismiss(animated: true, completion: nil)
        let moreDetailVC = MoreDetailViewController()
        mvdelegate?.selectedCell(vc: moreDetailVC, title: title)
        AppService.callApiXML(url: url, callback: {item in
            moreDetailVC.binData(arr: item, title: title)
        })
    }
}
