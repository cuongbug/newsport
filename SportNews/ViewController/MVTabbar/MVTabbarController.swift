//
//  MVTabbarController.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit
import ESTabBarController_swift

class MVTabbarController: ESTabBarController, UITabBarControllerDelegate {
    
    var titleHome = ""
    var titleNews = ""
    var titleVideo = ""
    var titleSetting = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        notiAction()
        changeLanguage()
        setTitleTabbar()
        UITabbar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func setTitleTabbar() {
        titleHome = localizedString(key: "item_home_tabbar")
        titleNews = localizedString(key: "item_news_tabbar")
        titleVideo = localizedString(key: "item_video_tabbar")
        titleSetting = localizedString(key: "item_setting_tabbar")
    }
    func UITabbar() {
        self.tabBar.sizeThatFits(CGSize(width: 10, height: 10))
        let vc1 = UIStoryboard.init(name: "MVTabbarController", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController")
        let vc2 = UIStoryboard.init(name: "MVTabbarController", bundle: nil).instantiateViewController(withIdentifier: "SportNewsViewController")
        let vc3 = UIStoryboard.init(name: "MVTabbarController", bundle: nil).instantiateViewController(withIdentifier: "VideoViewController")
        let vc4 = UIStoryboard.init(name: "MVTabbarController", bundle: nil).instantiateViewController(withIdentifier: "LiveScoreViewController")
        
        
        vc1.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: titleHome, image: UIImage(named: "ic_home"), selectedImage: UIImage(named: "ic_home_select"))
        vc2.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: titleNews, image: UIImage(named: "ic_news"), selectedImage: UIImage(named: "ic_news_select"))
        vc3.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: titleVideo, image: UIImage(named: "ic_video"), selectedImage: UIImage(named: "ic_video_select"))
        vc4.tabBarItem = ESTabBarItem.init(ExampleBouncesContentView(), title: titleSetting, image: UIImage(named: "ic_setting_tabbar"), selectedImage: UIImage(named: "ic_setting_selected"))
        
        self.viewControllers = [vc1, vc2, vc3, vc4]
        setColorTabbar()
    }
    func notiAction() {
        NotificationCenter.default.addObserver(self, selector: #selector(changeColor), name: NSNotification.Name("updateColor"), object: nil)
    }
    func changeLanguage() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateLanguage), name: NSNotification.Name("updateLanguage"), object: nil)
    }
    
    @objc func changeColor() {
        print("TestNoti")
        setColorTabbar()
    }
    func setColorTabbar(){
        if Utils.getCheckDarkMode() {
            self.tabBar.barStyle = .black
            self.tabBar.backgroundColor = UIColor.black
        } else {
            self.tabBar.barStyle = .black
            self.tabBar.backgroundColor = UIColor.mainColor
        }
    }
    @objc func updateLanguage() {
        setTitleTabbar()
        self.tabBar.items?[0].title = titleHome
        self.tabBar.items?[1].title = titleNews
        self.tabBar.items?[2].title = titleVideo
        self.tabBar.items?[3].title = titleSetting
    }
}
extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        if ScreenSizeApp.checkDevice() == .iphoneX || ScreenSizeApp.checkDevice() == .iphoneXMax {
            sizeThatFits.height = 85
        } else {
            sizeThatFits.height = 45
        }
        return sizeThatFits
    }
}
