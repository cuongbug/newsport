//
//  VideoViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let presenter = HomePresenter()
    var arrDataVideo: [Item] = []
    let refreshControl = UIRefreshControl()
    let interactor = Interactor()
    
    @IBOutlet weak var labelVideoFootball: UILabel!
    @IBOutlet weak var headerView: CustomHeaderView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeader)
        self.navigationController?.isNavigationBarHidden = true
        setDataTableView()
        DispatchQueue.global(qos: .userInteractive).async {
            self.setParams()
        }
        changeDarkMode()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        labelVideoFootball.text = localizedString(key: "title_football_video_videovc")
        headerView.backgroundColor = UIColor.mainColor
        self.view.backgroundColor = .whiteColor
        tableView.backgroundColor = .whiteColor
        addRefresh()
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrDataVideo.count > 0 {
            return arrDataVideo.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height / 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell", for: indexPath) as! VideoTableViewCell
            if arrDataVideo.count > 0 {
                cell.binData(arr: arrDataVideo, index: indexPath.row)
            }
        return cell
    }
    //MARK : - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.gotoDetailVC(link: arrDataVideo[indexPath.row].link!, date: arrDataVideo[indexPath.row].pubDate!, titleNews: arrDataVideo[indexPath.row].title!, title: "VIDEO", urlImage: arrDataVideo[indexPath.row].urlImage!, vc: self)
        tableView.reloadData()
    }
    //MARK: - Presenter
    func setParams() {
        presenter.requestToModel(url: AppService.urlVideo, callback: {data, isColect  in
            self.arrDataVideo = data
            self.tableView.reloadData()
        })
    }
    private func changeDarkMode() {
           NotificationCenter.default.addObserver(self, selector: #selector(updateDarkMode), name: NSNotification.Name("updateDarkMode"), object: nil)
       }
       @objc func updateDarkMode() {
           self.tableView.reloadData()
       }
    // refreshControl
      func addRefresh() {
          refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkColor])
          refreshControl.backgroundColor = UIColor.clear
          refreshControl.tintColor = UIColor.darkColor

          refreshControl.addTarget(self, action: #selector(methodPullToRefresh(sender:)), for: UIControl.Event.valueChanged)

          self.tableView.addSubview(refreshControl)

      }

      @objc func methodPullToRefresh(sender: AnyObject)
      {
          setParams()
          DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
              self.refreshControl.endRefreshing()
          })

      }
    
    
    //MARK - Action
    @IBAction func screenEdgePan(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .right)
        
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: Interactor()){
                self.performSegue(withIdentifier: "openMenuFromVideo", sender: nil)
        }
    }
    @IBAction func openMenu(_ sender: UIButton) {
        performSegue(withIdentifier: "openMenuFromVideo", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = Interactor()
            destinationViewController.mvdelegate = self
        }
    }
}
extension VideoViewController: UIViewControllerTransitioningDelegate, MenuViewControllerDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    func selectedCell(vc: MoreDetailViewController, title: String) {
        vc.binDataTitle(title: title)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
