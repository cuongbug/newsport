//
//  LoadMoreTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/11/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class LoadMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var loaddingControl: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
