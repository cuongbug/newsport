//
//  VideoTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/25/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var customView: CustomView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arr: [Item], index: Int) {
        self.imageDetail = UIImage.getImage(url: arr[index].urlImage!, imageDetail: self.imageDetail)
        self.labelTitle.text = arr[index].title
        self.labelTitle.textColor = .darkColor
        self.labelDate.text = arr[index].pubDate
        self.labelDate.textColor = .whiteColor
        self.customView.backgroundColor = .whiteColor
    }
    
}
