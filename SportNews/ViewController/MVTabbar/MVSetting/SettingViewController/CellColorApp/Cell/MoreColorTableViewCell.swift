//
//  MoreColorTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class MoreColorTableViewCell: UITableViewCell {

    @IBOutlet weak var imageChecked: UIImageView!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var titleColor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arr: [String], stringColor: [String], index: Int, indexColor: Int) {
        self.titleColor.textColor = .darkColor
        self.contentView.backgroundColor = .whiteColor
        self.titleColor.text = arr[index]
        self.viewColor.backgroundColor = UIColor.init(hexString: stringColor[index])
        self.viewColor.layer.cornerRadius = 4
        if index == indexColor {
            self.imageChecked.isHidden = false
        } else {
            self.imageChecked.isHidden = true
        }
    }
    
}
