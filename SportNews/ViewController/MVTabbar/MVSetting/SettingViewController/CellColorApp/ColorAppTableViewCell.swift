//
//  ColorAppTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

protocol ColorAppTableViewCellDelegate {
    func selectChangeColor()
}
class ColorAppTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageDow: UIImageView!
    var arrItemColor: [String] = []
    var arrHexColor: [String] = []
    var indexColor: Int = 0
    var mvdelegate: ColorAppTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setDataTableView()
    }
    func setDataTableView() {
        tableView.isScrollEnabled = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "MoreColorTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreColorTableViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arr: [String], arrColor: [String], selected: Bool) {
        self.labelTitle.textColor = .darkColor
        labelTitle.text = localizedString(key: "label_color_app_settingvc")
        self.contentView.backgroundColor = .whiteColor
        self.arrItemColor = arr
        self.arrHexColor = arrColor
        if selected {
            imageDow.image = UIImage.init(named: "ic_up_setting_black")
        } else {
            imageDow.image = UIImage.init(named: "ic_down_setting_black")
        }
        tableView.reloadData()
    }
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItemColor.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreColorTableViewCell", for: indexPath) as! MoreColorTableViewCell
        self.indexColor = self.getIndexColor()
        cell.binData(arr: arrItemColor, stringColor: arrHexColor, index: indexPath.row, indexColor: indexColor)
        return cell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Utils.setHexColor(hexString: arrHexColor[indexPath.row])
        self.mvdelegate?.selectChangeColor()
        self.saveIndexColor(index: indexPath.row)
        tableView.reloadData()
    }
    private func saveIndexColor(index: Int) {
        UserDefaults.standard.set(index, forKey: "indexColor")
    }
    private func getIndexColor() -> Int {
        UserDefaults.standard.integer(forKey: "indexColor")
    }
    
}
