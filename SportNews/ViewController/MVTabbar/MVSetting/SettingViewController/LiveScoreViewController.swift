//
//  LiveScoreViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/24/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 10.0, *)
class LiveScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightHeaderView: NSLayoutConstraint!
    @IBOutlet weak var headerView: CustomHeaderView!
    
    @IBOutlet weak var labelTitleSetting: UILabel!
    var itemFbSave: [ItemFacebook] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var itemSave: [ItemSave] = []
    var itemSection: [String] = []
    var itemColorApp: [String] = ["Xanh Nước Biển", "Đỏ Lửa", "Tím Thủy Chung", "Vàng Chanh", "Hồng Cá Tính", "Đen"]
    var hexColor: [String] = ["#4693B8", "#DC143C", "#663399", "#FFD700", "#FFDAB9", "#696969"]
    var selected: Bool = false
    var selectedRow1: Bool = false
    var selectedRow2: Bool = false
    var selectedRow3: Bool = false
    var selectedRow4: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeaderView)
        self.setDataTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        let sectionLogin = localizedString(key: "label_login_settingvc")
        let sectionActivate = localizedString(key: "label_activate_settingvc")
        let sectionCustomSetting = localizedString(key: "label_custom_settingvc")
        let sectionContact = localizedString(key: "label_contact_settingvc")
        let sectionMoreApp = localizedString(key: "label_moreapp_settingvc")
        itemSection = [sectionLogin, sectionActivate, sectionCustomSetting, sectionContact, sectionMoreApp]
        
        let color0 = localizedString(key: "label_color_blue")
        let color1 = localizedString(key: "label_color_red")
        let color2 = localizedString(key: "label_color_purple")
        let color3 = localizedString(key: "label_color_yellow")
        let color4 = localizedString(key: "label_color_pink")
        let color5 = localizedString(key: "label_color_brown")
        itemColorApp = [color0, color1, color2, color3, color4, color5]
        labelTitleSetting.text = localizedString(key: "title_setting_settingvc")
        if Utils.getLoginFb() {
            self.getInfoFbFromCore()
        }
        headerView.backgroundColor = UIColor.mainColor
        fetchData()
        tableView.reloadData()
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "LiveScoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveScoreTableViewCell")
        tableView.register(UINib(nibName: "ColorAppTableViewCell", bundle: nil), forCellReuseIdentifier: "ColorAppTableViewCell")
        tableView.register(UINib(nibName: "DarkNightTableViewCell", bundle: nil), forCellReuseIdentifier: "DarkNightTableViewCell")
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        tableView.register(UINib(nibName: "LoginTableViewCell", bundle: nil), forCellReuseIdentifier: "LoginTableViewCell")
        tableView.register(UINib(nibName: "MoreAppTableViewCell", bundle: nil), forCellReuseIdentifier: "MoreAppTableViewCell")
        tableView.register(UINib(nibName: "MultiLangTableViewCell", bundle: nil), forCellReuseIdentifier: "MultiLangTableViewCell")
    }
    func fetchData() {
        do {
            itemSave = try
            context.fetch(ItemSave.fetchRequest())
            print(itemSave.count)
            DispatchQueue.main.async {
                print(self.itemSave.count)
                self.tableView.reloadData()
            }
        } catch {
            print("Couldn't Fetch Data")
        }
    }
    //MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemSection.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader: SettingHeaderView = Bundle.main.loadNibNamed("SettingHeaderView", owner: self, options: nil)?.first as! SettingHeaderView
        viewHeader.binData(arrItem: itemSection, index: section)
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 4
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 187
        } else if indexPath.section == 1 {
            if selected == false {
                return 44
            }
            return CGFloat(44 + 80*itemSave.count)
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                if selectedRow1 {
                    return CGFloat(44 + 44*(itemColorApp.count) - 1)
                }
                return 44
            } else if indexPath.row == 1 {
                if selectedRow2 {
                    return 88
                }
                return 44
            } else if indexPath.row == 2 {
                if selectedRow3 {
                    return 88
                }
                return 44
            } else if indexPath.row == 3 {
                if selectedRow4 {
                    return 88
                }
               return 44
            } else {
                return 44
            }
        } else if indexPath.section == 3 {
            return 180
        } else if indexPath.section == 4 {
            return 120
        } else {
            return 44
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoginTableViewCell", for: indexPath) as! LoginTableViewCell
            cell.mvdelegate = self
                cell.binData(arrFbSave: self.itemFbSave)
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LiveScoreTableViewCell", for: indexPath) as! LiveScoreTableViewCell
            cell.mvdelegate = self
            cell.binData(arr: itemSave, selected: selected)
            return cell
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ColorAppTableViewCell", for: indexPath) as! ColorAppTableViewCell
                cell.mvdelegate = self
                cell.binData(arr: itemColorApp, arrColor: hexColor, selected: selectedRow1)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DarkNightTableViewCell", for: indexPath) as! DarkNightTableViewCell
                cell.binData(selected: selectedRow2)
                cell.mvdelegate = self
                return cell
            } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
                cell.binData(selected: selectedRow3)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MultiLangTableViewCell", for: indexPath) as! MultiLangTableViewCell
                cell.mvdelegate = self
                cell.binData(selectedCell: selectedRow4)
                return cell
            }
        } else if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
                cell.binData()
                return cell
        } else if indexPath.section == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreAppTableViewCell", for: indexPath) as! MoreAppTableViewCell
            cell.reloadData()
                return cell
        } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreAppTableViewCell", for: indexPath) as! MoreAppTableViewCell
                return cell
        }
        
    }
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if Utils.getLoginFb() {
//                Utils.fbLogout()
//                deleteInfoFb(entity: "ItemFacebook")
//                Utils.setLoginFb(login: false)
//                self.itemFbSave.removeAll()
//                tableView.reloadData()
            } else {
                Utils.fbLogin(vc: self, callback: { success in
                    if success {
                        self.getInfoFbFromCore()
                    }
                })
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                if itemSave.count == 0 {
                    Utils.showMessage(message: "Không có tin đã lưu", font: .systemFont(ofSize: 16))
                } else {
                    if selected {
                        self.selected = false
                    } else {
                        self.selected = true
                    }
                }
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                if selectedRow1 {
                    selectedRow1 = false
                } else {
                    selectedRow1 = true
                }
            } else if indexPath.row == 1 {
                if selectedRow2 {
                    selectedRow2 = false
                } else {
                    selectedRow2 = true
                }
            } else if indexPath.row == 2 {
                if selectedRow3 {
                    selectedRow3 = false
                } else {
                    selectedRow3 = true
                }
            } else if indexPath.row == 3 {
                if selectedRow4 {
                    selectedRow4 = false
                } else {
                    selectedRow4 = true
                }
            }
        }
        
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
//        tableView.reloadData()
    }
    
    // MARK: - GetDataFromCore
    func getInfoFbFromCore() {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            itemFbSave = try
            context.fetch(ItemFacebook.fetchRequest())
            print(itemFbSave.count)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Couldn't Fetch Data")
        }
    }
}
@available(iOS 10.0, *)
extension LiveScoreViewController: LiveScoreTableViewCellDelegate, ColorAppTableViewCellDelegate, DarkNightTableViewCellDelegate, MultiLangTableViewCellDelegate {
    // MARK : - LiveScoreTableViewCellDelegate
    func deleteCell() {
        fetchData()
        tableView.reloadData()
    }
    func selectedCell(arrSave: [ItemSave], index: Int) {
        let detailVC = DetailViewController()
        detailVC.binData(url: arrSave[index].linkWebView!, date: arrSave[index].pubDateSave!, titleNews: "", title: "Chi tiết", urlImage: arrSave[index].linkUrlSave!)
        self.navigationController?.present(detailVC, animated: true, completion: nil)
    }
    // MARK: - ColorAppTableViewCellDelegate
    func selectChangeColor() {
        if #available(iOS 13.0, *) {
        changeColorApp()
        }
    }
    // MARK: - DarkNightTableViewCellDelegate
    func selectDarkMode() {
        headerView.backgroundColor = UIColor.mainColor
        NotificationCenter.default.post(name: NSNotification.Name("updateDarkMode"), object: nil)
        changeColorApp()
    }
    func changeColorApp() {
//        self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?.first
        headerView.backgroundColor = UIColor.mainColor
        self.view.backgroundColor = .whiteColor
        tableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name("updateColor"), object: nil)
    }
    // MARK: - MultiLangTableViewCellDelegate
    func changeLanguage() {
        viewWillAppear(true)
        tableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name("updateLanguage"), object: nil)
    }
    
    
}
@available(iOS 10.0, *)
extension LiveScoreViewController: LoginTableViewCellDelegate {
    func logOut() {
        if Utils.getLoginFb() {
            Utils.fbLogout()
            Utils.deleteEntities(entity: "ItemFacebook")
            Utils.setLoginFb(login: false)
            self.itemFbSave.removeAll()
            tableView.reloadData()
        } else {
            Utils.fbLogin(vc: self, callback: { success in
                if success {
                    self.getInfoFbFromCore()
                }
            })
        }
    }
}
