//
//  SettingHeaderView..swift
//  SportNews
//
//  Created by ManhCuong on 3/3/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

class SettingHeaderView: CustomView {
    @IBOutlet weak var labelTitle: UILabel!
    
        override func awakeFromNib() {
        super.awakeFromNib()
        }
    func binData(arrItem: [String], index: Int) {
        self.labelTitle.text = arrItem[index]
        self.labelTitle.textColor = .darkColor
        self.backgroundColor = .whiteColor
    }
}
