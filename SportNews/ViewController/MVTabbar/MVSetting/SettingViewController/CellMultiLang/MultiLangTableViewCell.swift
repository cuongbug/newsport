//
//  MultiLangTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/9/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol MultiLangTableViewCellDelegate {
    func changeLanguage()
}

class MultiLangTableViewCell: UITableViewCell {
    
    var mvdelegate: MultiLangTableViewCellDelegate?
    @IBOutlet weak var imageSelect: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSelectLang: UILabel!
    @IBOutlet weak var segmentSelectLang: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelTitle.font = .sfProTextRegular(size: 16)
        labelSelectLang.font = .sfProTextRegular(size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(selectedCell: Bool) {
        if Utils.getLanguage() {
            segmentSelectLang.selectedSegmentIndex = 0
        } else {
            segmentSelectLang.selectedSegmentIndex = 1
        }
        if selectedCell {
            imageSelect.image = UIImage.init(named: "ic_up_setting_black")
        } else {
            imageSelect.image = UIImage.init(named: "ic_down_setting_black")
        }
        if #available(iOS 13.0, *) {
            segmentSelectLang.selectedSegmentTintColor = .mainColor
            segmentSelectLang.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        }
        self.contentView.backgroundColor = .whiteColor
        labelTitle.text = localizedString(key: "label_multilang_settingvc")
        labelTitle.textColor = .darkColor
        labelSelectLang.text = localizedString(key: "label_select_language_settingvc")
        labelSelectLang.textColor = .darkColor
    }
    
    
    @IBAction func onChangeLanguage(_ sender: UISegmentedControl) {
        if segmentSelectLang.selectedSegmentIndex == 0 {
            Utils.setLanguage(status: true)
        } else {
            Utils.setLanguage(status: false)
        }
        mvdelegate?.changeLanguage()
    }
    
}
