//
//  MoreAppTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/6/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
struct DataMoreApp {
    let nameApp: String?
    let imageApp: UIImage?
    init(name: String, image: UIImage) {
        self.nameApp = name
        self.imageApp = image
    }
}

class MoreAppTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var arrApp: [DataMoreApp] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        arrApp = [DataMoreApp.init(name: "Tin Tức Bóng Đá", image: UIImage(named: "ic_tin_tuc_bong_da")!),
                  DataMoreApp.init(name: "Bóng Đá Hôm Nay", image: UIImage(named: "ic_bong_da_hom_nay")!)]
        
        setDataTableView()
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "DetailMoreAppTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailMoreAppTableViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func reloadData() {
        self.contentView.backgroundColor = .whiteColor
        tableView.reloadData()
    }
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrApp.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailMoreAppTableViewCell", for: indexPath) as! DetailMoreAppTableViewCell
        cell.binData(arrData: arrApp, index: indexPath.row)
        return cell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            UIApplication.shared.openURL(URL(string: "itms-apps://apps.apple.com/vn/app/b%C3%B3ng-%C4%91%C3%A1-h%C3%B4m-nay-tr%E1%BB%B1c-ti%E1%BA%BFp-b%C3%B3ng/id1435277539?l=vi")!)
        }
    }
}
