//
//  DetailMoreAppTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/6/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class DetailMoreAppTableViewCell: UITableViewCell {
    @IBOutlet weak var iconApp: UIImageView!
    @IBOutlet weak var titleApp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconApp.layer.cornerRadius = 4
        iconApp.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arrData: [DataMoreApp], index: Int) {
        self.contentView.backgroundColor = .whiteColor
        iconApp.backgroundColor = .white
        self.titleApp.textColor = .darkColor
        self.iconApp.image = arrData[index].imageApp
        self.titleApp.text = arrData[index].nameApp
    }
    
}
