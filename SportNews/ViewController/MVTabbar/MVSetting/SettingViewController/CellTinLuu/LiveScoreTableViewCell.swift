//
//  LiveScoreTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 2/28/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol LiveScoreTableViewCellDelegate {
    func deleteCell()
    func selectedCell(arrSave: [ItemSave], index: Int)
}

@available(iOS 10.0, *)
class LiveScoreTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageDow: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var mvdelegate: LiveScoreTableViewCellDelegate?
    var arrItem: [ItemSave] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        setDataTableView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDataTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "CellHotNews1TableViewCell", bundle: nil), forCellReuseIdentifier: "CellHotNews1TableViewCell")
    }
    func binData(arr: [ItemSave], selected: Bool) {
        self.contentView.backgroundColor = .whiteColor
        self.labelTitle.textColor = .darkColor
        labelTitle.text = localizedString(key: "label_news_saved_settingvc")
        self.arrItem = arr
        if selected {
            imageDow.image = UIImage.init(named: "ic_up_setting_black")
        } else {
            imageDow.image = UIImage.init(named: "ic_down_setting_black")
        }
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItem.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHotNews1TableViewCell") as! CellHotNews1TableViewCell
        if arrItem.count > 0 {
            cell.binDataToItemSave(arr: arrItem, index: indexPath.row)
        }
        return cell
    }
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mvdelegate?.selectedCell(arrSave: arrItem, index: indexPath.row)
        tableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let item = self.arrItem[indexPath.row]
            self.context.delete(item)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            self.arrItem.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.mvdelegate?.deleteCell()
            
        }
        
        let share = UITableViewRowAction(style: .default, title: "Share") { (action, indexPath) in
            // delete item at indexPath
            
            print("Share")
            
        }
        
        delete.backgroundColor = UIColor(red: 0/255, green: 177/255, blue: 106/255, alpha: 1.0)
        share.backgroundColor = UIColor(red: 54/255, green: 215/255, blue: 183/255, alpha: 1.0)
        
        return [delete,share]
    }
    
}
