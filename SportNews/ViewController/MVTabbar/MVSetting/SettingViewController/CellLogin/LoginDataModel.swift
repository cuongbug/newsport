//
//  LoginDataModel.swift
//  SportNews
//
//  Created by ManhCuong on 3/4/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import Foundation
import UIKit

struct LoginDataModel {
    var urlAvatar: String
    var name: String
    var email: String
    var birthDay: String
    
    init(urlAvatar: String, name: String, email: String, birthDay: String) {
        self.urlAvatar = urlAvatar
        self.name = name
        self.email = email
        self.birthDay = birthDay
    }
}
