//
//  LoginTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/3/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol LoginTableViewCellDelegate {
    func logOut()
}

class LoginTableViewCell: UITableViewCell {
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelLogout: UILabel!
    @IBOutlet weak var logOutButton: UIButton!
    
    var mvdelegate: LoginTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(arrFbSave: [ItemFacebook]) {
        self.contentView.backgroundColor = .whiteColor
        labelName.textColor = .darkColor
        if Utils.getLoginFb() && arrFbSave.count > 0 {
            self.imageAvatar = UIImage.getImage(url: arrFbSave[0].urlAvatar!, imageDetail: self.imageAvatar)
            self.labelName.text = arrFbSave[0].name
            self.labelName.font = .sfProTextBold(size: 24)
            self.labelLogout.text = localizedString(key: "label_logout_settingvc")
            self.imageAvatar.clipsToBounds = true
            self.imageAvatar.layer.cornerRadius = 4
        } else {
            self.labelName.font = .sfProTextRegular(size: 14)
            self.imageAvatar.image = UIImage.init(named: "ic_avatar_fb")
            self.labelName.text = localizedString(key: "label_please_notifi_settingvc")
            self.labelLogout.text = localizedString(key: "label_login_withFB_settingvc")
        }
    }
    
    
    @IBAction func logOut(_ sender: Any) {
        mvdelegate?.logOut()
    }
    
}
