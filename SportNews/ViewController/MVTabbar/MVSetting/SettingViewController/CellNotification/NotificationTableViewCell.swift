//
//  NotificationTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var switchNotifi: UISwitch!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var imageDow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .whiteColor
        switchNotifi.backgroundColor = UIColor.lightGray
        switchNotifi.onTintColor = UIColor.mainColor
        switchNotifi.layer.cornerRadius = switchNotifi.frame.size.height / 2
        switchNotifi.layer.borderColor = UIColor.white.cgColor
        switchNotifi.layer.borderWidth = 0.3
        switchNotifi.isOn = CustomNotification.getStatusNotification()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(selected: Bool) {
        self.contentView.backgroundColor = .whiteColor
        self.labelTitle.textColor = .darkColor
        self.labelContent.textColor = .darkColor
        labelTitle.text = localizedString(key: "label_notifi_settingvc")
        labelContent.text = localizedString(key: "label_take_notifi_settingvc")
        if selected {
            imageDow.image = UIImage.init(named: "ic_up_setting_black")
        } else {
            imageDow.image = UIImage.init(named: "ic_down_setting_black")
        }
        
    }
    @IBAction func turnOnSwitchNotifi(_ sender: UISwitch) {
        if CustomNotification.getStatusNotification() {
            CustomNotification.openSettingURL()
            return
        }
        CustomNotification.registerNotification()
    }
    
}
