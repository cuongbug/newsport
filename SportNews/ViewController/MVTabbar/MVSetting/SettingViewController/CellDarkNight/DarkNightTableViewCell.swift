//
//  DarkNightTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/2/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit
protocol DarkNightTableViewCellDelegate {
    func selectDarkMode()
}

class DarkNightTableViewCell: UITableViewCell {
    var mvdelegate: DarkNightTableViewCellDelegate?

    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageDow: UIImageView!
    @IBOutlet weak var darkNightSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        darkNightSwitch.isOn = Utils.getCheckDarkMode()
        darkNightSwitch.backgroundColor = UIColor.lightGray
        darkNightSwitch.onTintColor = UIColor.init(hexString: "#202020")
        darkNightSwitch.layer.cornerRadius = darkNightSwitch.frame.size.height / 2
        darkNightSwitch.layer.borderColor = UIColor.white.cgColor
        darkNightSwitch.layer.borderWidth = 0.3
        // Initialization code
    }

    func binData(selected: Bool) {
        labelTitle.textColor = .darkColor
        labelContent.textColor = .darkColor
        labelTitle.text = localizedString(key: "label_view_mode_settingvc")
        labelContent.text = localizedString(key: "label_view_dark_mode_settingvc")
        self.contentView.backgroundColor = .whiteColor
        if selected {
            imageDow.image = UIImage.init(named: "ic_up_setting_black")
        } else {
            imageDow.image = UIImage.init(named: "ic_down_setting_black")
        }
    }
    
    @IBAction func turnOnDarkNight(_ sender: UISwitch) {
        if darkNightSwitch.isOn == true {
            Utils.checkDarkMode(isOn: true)
            Utils.setDarkColor(hexString: "#202020")
            Utils.setBaseColor(hexColor: Utils.getDarkColor())
            Utils.setWhiteColor(hexString: "#202020")
            Utils.setDarkColor(hexString: "#FFFFFF")
        } else {
            Utils.setBaseColor(hexColor: Utils.getColorApp())
            Utils.checkDarkMode(isOn: false)
            Utils.setDarkColor(hexString: "#202020")
            Utils.setWhiteColor(hexString: "#FFFFFF")
        }
        mvdelegate?.selectDarkMode()
    }
    
}

