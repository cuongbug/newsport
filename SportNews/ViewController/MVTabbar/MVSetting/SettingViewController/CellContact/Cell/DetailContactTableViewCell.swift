//
//  DetailContactTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/6/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class DetailContactTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    let arrTitle: [String] = ["Góp ý & đánh giá ứng dụng", "Thông tin ứng dụng", "Liên hệ quảng cáo"]
    let arrContent: [String] = ["Mời bạn góp ý giúp chúng tôi hoàn thiện tốt hơn", "Ứng dụng tin tức bóng đá \nCập nhập kết quả và lịch thi đấu", "Phone: 0966982943 (Hà Nội) \nEmail: cuongdodev@gmail.com (Hà Nội)"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(index: Int) {
        self.contentView.backgroundColor = .whiteColor
        labelContent.textColor = .darkColor
        labelTitle.textColor = .darkColor
        labelContent.text = arrContent[index]
        labelTitle.text = arrTitle[index]
    }
    
}
