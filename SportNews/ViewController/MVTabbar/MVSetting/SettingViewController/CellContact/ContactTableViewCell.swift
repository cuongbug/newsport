//
//  ContactTableViewCell.swift
//  SportNews
//
//  Created by ManhCuong on 3/3/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setData()
    }

    func setData() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "DetailContactTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailContactTableViewCell")
    }
    func binData() {
        self.contentView.backgroundColor = .whiteColor
        self.tableView.reloadData()
    }
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailContactTableViewCell", for: indexPath) as! DetailContactTableViewCell
        cell.binData(index: indexPath.row)
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            if let url = URL(string: "tel://0966982943"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
        tableView.reloadData()
    }
    
}
