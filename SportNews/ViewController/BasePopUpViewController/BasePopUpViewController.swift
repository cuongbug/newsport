//
//  BasePopUpViewController.swift
//  SportNews
//
//  Created by ManhCuong on 3/12/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class BasePopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func showPopUp(with VC: UIViewController) {
        VC.view.frame = self.view.frame
        VC.willMove(toParent: self)
        self.view.addSubview(VC.view)
        self.addChild(VC)
        VC.didMove(toParent: self)
    }
}
