//
//  MoreDetailViewController.swift
//  SportNews
//
//  Created by ManhCuong on 2/27/20.
//  Copyright © 2020 encodejsc. All rights reserved.
//

import UIKit

class MoreDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var headerView: CustomHeaderView!
    @IBOutlet weak var heightHeaderView: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelTitle: UILabel!
    
    let refreshControl = UIRefreshControl()
    var arrData: [Item] = []
    var titleDetail: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.labelTitle.text = titleDetail
        self.addSwipLeft()
        ScreenSizeApp.setContraintHeader(layoutContraint: heightHeaderView)
        setDataCollectionView()
        addRefresh()
    }
    override func viewWillAppear(_ animated: Bool) {
        headerView.backgroundColor = UIColor.mainColor
    }
    func setDataCollectionView() {
        collectionView.backgroundColor = UIColor.whiteColor
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "CollectionHotNewsCell", bundle: nil), forCellWithReuseIdentifier: "CollectionHotNewsCell")
    }

    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrData.count > 0 {
            return arrData.count
        }
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = (UIScreen.main.bounds.width - 48) / 2
        let height: CGFloat = UIScreen.main.bounds.width / 2
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionHotNewsCell", for: indexPath) as! CollectionHotNewsCell
        if arrData.count > 0 {
            cell.binData(arr: arrData, index: indexPath.row)
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = DetailViewController()
        let arrIndex = arrData[indexPath.row]
        detailVC.binData(url: arrIndex.link!, date: arrIndex.pubDate!, titleNews: arrIndex.title!, title: localizedString(key: "label_detail_detail_vc"), urlImage: arrIndex.urlImage!)
        detailVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func binData(arr: [Item], title: String) {
        self.arrData = arr
        if let collectionView = self.collectionView {
            collectionView.reloadData()
        }
    }
    func binDataTitle(title: String) {
        self.titleDetail = title
    }
    // refreshControl
    func addRefresh() {
        refreshControl.attributedTitle = NSAttributedString.init(string: "Loading...", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkColor, NSAttributedString.Key.font: UIFont.sfProTextRegular(size: 16)])
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.darkColor

        refreshControl.addTarget(self, action: #selector(methodPullToRefresh(sender:)), for: UIControl.Event.valueChanged)

        self.collectionView.addSubview(refreshControl)

    }

    @objc func methodPullToRefresh(sender: AnyObject)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
            self.refreshControl.endRefreshing()
        })

    }
    
}
